package rss.test.com.rss;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import rss.test.com.rss.helpers.AppPreferences;


public class LoginActivity extends ActionBarActivity {

    private static final String LOGIN = "123";
    private static final String PASSWORD = "123";

    private EditText login;
    private EditText password;
    private View submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.login = (EditText)this.findViewById(R.id.login);
        this.password = (EditText)this.findViewById(R.id.password);
        this.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }

    private void submit() {
        final String loginInput = this.login.getText().toString();
        final String passwordInput = this.password.getText().toString();
        if(verifyInput(loginInput, passwordInput)) {
            if(loginInput.equals(LOGIN) && passwordInput.equals(PASSWORD)) {
                AppPreferences.getInstance().setToken("some_token");
                this.setResult(Activity.RESULT_OK);
                this.finish();
            }
            else {
                this.login.setError(this.getString(R.string.user_not_found_or_incorrect_password));
            }
        }
    }

    private boolean verifyInput(final String loginInput, final String passwordInput) {
        boolean valid = true;
        if(TextUtils.isEmpty(loginInput)) {
            valid = false;
            this.login.setError(this.getString(R.string.login_required));
        }
        if(TextUtils.isEmpty(passwordInput)) {
            valid = false;
            this.password.setError(this.getString(R.string.password_required));
        }
        return valid;
    }

}
