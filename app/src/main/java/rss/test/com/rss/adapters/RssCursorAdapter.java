package rss.test.com.rss.adapters;

import android.content.Context;
import android.database.Cursor;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import rss.test.com.rss.R;
import rss.test.com.rss.db.tables.RssTable;

public class RssCursorAdapter extends CursorAdapter {

    private static SimpleDateFormat formatter = new SimpleDateFormat("F MMM yyyy H:mm:ss");

    public RssCursorAdapter(final Context context, final Cursor c) {
        super(context, c);
    }

    @Override
    public View newView(final Context context, final Cursor cursor, final ViewGroup viewGroup) {
        return View.inflate(context, R.layout.list_item_rss, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final TextView title = (TextView)view.findViewById(R.id.title);
        final TextView date = (TextView)view.findViewById(R.id.date);
        final TextView description = (TextView)view.findViewById(R.id.description);

        title.setText(cursor.getString(cursor.getColumnIndex(RssTable.COLUMN_TITLE)));
        final long time = cursor.getLong(cursor.getColumnIndex(RssTable.COLUMN_DATE));
        date.setText(formatter.format(new Date(time)));
        description.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndex(RssTable.COLUMN_DESCRIPTION))));
    }

}
