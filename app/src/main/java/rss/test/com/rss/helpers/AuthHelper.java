package rss.test.com.rss.helpers;

import android.text.TextUtils;

public class AuthHelper {

    public static boolean isAuth() {
        return !TextUtils.isEmpty(AppPreferences.getInstance().getToken());
    }

    private AuthHelper() { }

}
