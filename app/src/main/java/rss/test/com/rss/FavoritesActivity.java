package rss.test.com.rss;

import android.util.Pair;

import rss.test.com.rss.db.tables.RssTable;

public class FavoritesActivity extends BaseRssListActivity {

    @Override
    protected int getLayoutId() {
        return R.layout.activity_favorites;
    }

    @Override
    protected Pair<String, String[]> getCondition() {
        return new Pair<>(RssTable.COLUMN_FAVORITE + " = ?", new String[] { "1" });
    }

}
