package rss.test.com.rss;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;

import rss.test.com.rss.helpers.AppPreferences;
import rss.test.com.rss.helpers.AuthHelper;
import rss.test.com.rss.helpers.RssHelper;

public class MainActivity extends BaseRssListActivity implements SwipeRefreshLayout.OnRefreshListener {

    private static final int LOGIN_REQUEST = 1;

    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.swipeRefreshLayout = (SwipeRefreshLayout)this.findViewById(R.id.refresh);
        this.swipeRefreshLayout.setOnRefreshListener(this);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Pair<String, String[]> getCondition() {
        return new Pair<>(null, null);
    }

    @Override
    public void onRefresh() {
        this.swipeRefreshLayout.setRefreshing(true);
        RssHelper.fetch(this, new OnRssRefreshListener() {
            @Override
            public void onComplete() {
                MainActivity.this.swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(AuthHelper.isAuth()) {
            getMenuInflater().inflate(R.menu.menu_auth_main, menu);
        }
        else {
            getMenuInflater().inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_login:
                startActivityForResult(new Intent(this, LoginActivity.class), LOGIN_REQUEST);
                return true;
            case R.id.action_logout:
                AppPreferences.getInstance().setToken(null);
                this.invalidateOptionsMenu();
                return true;
            case R.id.action_favorites:
                startActivity(new Intent(this, FavoritesActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOGIN_REQUEST:
                if(resultCode == Activity.RESULT_OK) {
                    this.invalidateOptionsMenu();
                }
                break;
        }
    }
}
