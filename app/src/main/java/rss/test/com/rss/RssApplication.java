package rss.test.com.rss;

import android.app.Application;

import rss.test.com.rss.helpers.AppPreferences;
import rss.test.com.rss.helpers.RssHelper;

public class RssApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //update on start
        RssHelper.fetch(this);

        AppPreferences.init(this);
    }
}
