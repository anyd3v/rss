package rss.test.com.rss.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AppPreferences {

    private static final String TOKEN = "token";

    private static AppPreferences instance;

    private SharedPreferences sharedPrefs;

    public static void init(Context context) {
        instance = new AppPreferences(context);
    }

    public static AppPreferences getInstance() {
        return instance;
    }

    private AppPreferences(Context context) {
        this.sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public void setToken(final String token) {
        this.sharedPrefs.edit().putString(TOKEN, token).apply();
    }

    public String getToken() {
        return this.sharedPrefs.getString(TOKEN, null);
    }

}
