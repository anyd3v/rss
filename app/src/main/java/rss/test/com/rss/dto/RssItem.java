package rss.test.com.rss.dto;

public class RssItem {

    private String title;
    private String description;
    private long date;
    private String link;

    public RssItem(final String title, final String description, final long date, String link) {
        this.title = title;
        this.description = description;
        this.date = date;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }
}
