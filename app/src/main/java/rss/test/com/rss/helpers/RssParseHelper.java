package rss.test.com.rss.helpers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import rss.test.com.rss.dto.RssItem;

public class RssParseHelper {

    private static SimpleDateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

    public static List<RssItem> readFeed(final InputStream inputStream) {
        final List<RssItem> rssItems = new LinkedList<>();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(inputStream);

            NodeList items = doc.getElementsByTagName("item");

            for (int i = 0; i < items.getLength(); i++) {
                Element item = (Element)items.item(i);
                final String date = getValue(item, "pubDate");
                final RssItem rssItem = new RssItem(
                        getValue(item, "title"),
                        getValue(item, "description"),
                        formatter.parse(date).getTime(),
                        getValue(item, "link"));
                rssItems.add(rssItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rssItems;
    }

    private static String getValue(Element parent, String nodeName) {
        return parent.getElementsByTagName(nodeName).item(0).getFirstChild().getNodeValue();
    }

    private RssParseHelper() { }

}
