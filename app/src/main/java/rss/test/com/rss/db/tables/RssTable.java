package rss.test.com.rss.db.tables;

import android.database.sqlite.SQLiteDatabase;

public class RssTable {

    public static final String TABLE_NAME = "rss";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_FAVORITE = "favorite";

    public static final String[] PROJECTION = { COLUMN_ID, COLUMN_TITLE, COLUMN_DESCRIPTION,
                                                COLUMN_LINK, COLUMN_DATE, COLUMN_FAVORITE};

    private static String CREATE;

    static {
        CREATE = new StringBuilder()
                .append("CREATE TABLE ").append(TABLE_NAME).append("(")
                    .append(COLUMN_ID).append(" integer primary key,")
                    .append(COLUMN_TITLE).append(" text,")
                    .append(COLUMN_DESCRIPTION).append(" text,")
                    .append(COLUMN_LINK).append(" int8,")
                    .append(COLUMN_DATE).append(" text,")
                    .append("UNIQUE(").append(COLUMN_LINK).append(") ON CONFLICT IGNORE")
                .append(")")
                .toString();
    }

    public static void onCreate(final SQLiteDatabase database) {
        database.execSQL(CREATE);
    }

    public static void addFavoriteColumn(final SQLiteDatabase database) {
        database.execSQL(String.format("alter table %s add column %s int", TABLE_NAME, COLUMN_FAVORITE));
    }

}
