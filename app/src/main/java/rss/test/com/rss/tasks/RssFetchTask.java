package rss.test.com.rss.tasks;

import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import rss.test.com.rss.OnRssRefreshListener;
import rss.test.com.rss.converters.RssItemConverter;
import rss.test.com.rss.db.providers.RssItemsProvider;
import rss.test.com.rss.dto.RssItem;
import rss.test.com.rss.helpers.RssParseHelper;

public class RssFetchTask extends AsyncTask<String, Void, Void> {

    private Context context;
    private OnRssRefreshListener listener;

    public RssFetchTask(final Context context) {
        this(context, null);
    }

    public RssFetchTask(final Context context, final OnRssRefreshListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected Void doInBackground(String... urls) {
        for(String feedUrl: urls) {
            try {
                final URL url = new URL(feedUrl);
                final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("GET");
                connection.setDoInput(true);

                connection.connect();

                final List<RssItem> items = RssParseHelper.readFeed(connection.getInputStream());
                final ContentResolver contentResolver = this.context.getContentResolver();
                for(final RssItem item: items) {
                    contentResolver.insert(RssItemsProvider.CONTENT_URI, RssItemConverter.toContentValues(item));
                }
            }
            catch (final Exception ex) {
                Log.e("LOAD", "Load rss was failed", ex);
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        if (listener != null) {
            listener.onComplete();
        }
    }
}
