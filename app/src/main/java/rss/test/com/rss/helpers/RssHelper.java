package rss.test.com.rss.helpers;

import android.content.Context;
import android.os.AsyncTask;

import rss.test.com.rss.OnRssRefreshListener;
import rss.test.com.rss.tasks.RssFetchTask;

public class RssHelper {

    //any option for store
    private static final String[] URLS = { "http://habrahabr.ru/rss/hubs/" };

    public static void fetch(final Context context) {
        fetch(context, null);
    }

    public static void fetch(final Context context, final OnRssRefreshListener listener) {
        new RssFetchTask(context, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, URLS);
    }

    private RssHelper() { }

}
