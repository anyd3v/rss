package rss.test.com.rss.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

import rss.test.com.rss.db.tables.RssTable;

public class DbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "rss.db";
    private static final int DATABASE_VERSION = 2;

    private static List<Migration> MIGRATIONS = new LinkedList<Migration>() {{
        add(new Migration1());
        add(new Migration2());
    }};

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase database) {
        for(Migration migration: MIGRATIONS) {
            migration.apply(database);
        }
    }

    @Override
    public void onUpgrade(final SQLiteDatabase database, final int oldVersion, final int newVersion) {
        for(int i = oldVersion; i < newVersion; i++) {
            MIGRATIONS.get(i).apply(database);
        }
    }

    interface Migration {
        public void apply(SQLiteDatabase database);
    }

    private static class Migration1 implements Migration {
        @Override
        public void apply(final SQLiteDatabase database) {
            RssTable.onCreate(database);
        }
    }

    private static class Migration2 implements Migration {
        @Override
        public void apply(final SQLiteDatabase database) {
            RssTable.addFavoriteColumn(database);
        }
    }

}
