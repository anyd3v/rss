package rss.test.com.rss;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import rss.test.com.rss.db.tables.RssTable;


public class RssItemViewActivity extends ActionBarActivity {

    public static final String ITEM_URI = "item_uri";

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_item_view);

        this.webView = (WebView)this.findViewById(R.id.browser);

        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                view.loadUrl(url);
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        final Bundle extras = getIntent().getExtras();
        if(extras != null && extras.containsKey(ITEM_URI)) {
            final String uri = extras.getString(ITEM_URI);
            final Cursor cursor = this.getContentResolver().query(Uri.parse(uri), null, null, null, null);
            if(cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                final String url = cursor.getString(cursor.getColumnIndex(RssTable.COLUMN_LINK));
                if(!TextUtils.isEmpty(url)) {
                    this.webView.loadUrl(url);
                }
            }
        }
        else {
            this.finish();
        }
    }

}
