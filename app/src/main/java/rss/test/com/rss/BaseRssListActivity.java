package rss.test.com.rss;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import rss.test.com.rss.adapters.RssCursorAdapter;
import rss.test.com.rss.db.providers.RssItemsProvider;
import rss.test.com.rss.db.tables.RssTable;
import rss.test.com.rss.helpers.AuthHelper;

public abstract class BaseRssListActivity extends ActionBarActivity
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener{

    private ListView rssListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.rssListView = (ListView)this.findViewById(R.id.rss_list);

        this.rssListView.setOnItemClickListener(this);
        this.rssListView.setOnItemLongClickListener(this);

        this.getSupportLoaderManager().initLoader(1, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(final int i, final Bundle bundle) {
                final Pair<String, String[]> condition = getCondition();
                android.support.v4.content.CursorLoader cursorLoader = new android.support.v4.content.CursorLoader(
                        BaseRssListActivity.this,
                        RssItemsProvider.CONTENT_URI,
                        RssTable.PROJECTION,
                        condition.first,
                        condition.second,
                        null);
                return cursorLoader;
            }

            @Override
            public void onLoadFinished(final Loader<Cursor> cursorLoader, final Cursor cursor) {
                rssListView.setAdapter(new RssCursorAdapter(BaseRssListActivity.this, cursor));
            }

            @Override
            public void onLoaderReset(final Loader<Cursor> cursorLoader) {

            }
        });
    }

    abstract protected int getLayoutId();

    abstract protected Pair<String, String[]> getCondition();

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        final Intent showRssItem = new Intent(this, RssItemViewActivity.class)
                .putExtra(RssItemViewActivity.ITEM_URI, RssItemsProvider.CONTENT_URI + "/" + id);

        startActivity(showRssItem);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        if(AuthHelper.isAuth()) {
            final Uri uri = Uri.parse(RssItemsProvider.CONTENT_URI + "/" + id);
            final Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToFirst();
                final boolean isFavorite = cursor.getInt(cursor.getColumnIndex(RssTable.COLUMN_FAVORITE)) == 1;
                if (!isFavorite) {
                    showAddToFavoritePopup(uri);
                } else {
                    showRemoveFromFavoritePopup(uri);
                }
            }
            return true;
        }
        return false;
    }

    private void showRemoveFromFavoritePopup(final Uri uri) {
        showFavoritePopup(R.array.remove_to_favorites, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateFavoriteState(uri, false);
            }
        });
    }

    private void showAddToFavoritePopup(final Uri uri) {
        showFavoritePopup(R.array.add_to_favorites, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateFavoriteState(uri, true);
            }
        });
    }

    private void showFavoritePopup(final int itemsId, final DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setItems(itemsId, listener)
                .show();
    }

    private void updateFavoriteState(final Uri uri, final boolean value) {
        final ContentValues cv = new ContentValues();
        cv.put(RssTable.COLUMN_FAVORITE, value ? 1 : 0);
        this.getContentResolver().update(uri, cv, null, null);
    }

}
