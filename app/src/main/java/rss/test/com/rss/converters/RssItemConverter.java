package rss.test.com.rss.converters;

import android.content.ContentValues;

import rss.test.com.rss.db.tables.RssTable;
import rss.test.com.rss.dto.RssItem;

public class RssItemConverter {

    public static ContentValues toContentValues(final RssItem item) {
        final ContentValues cv = new ContentValues();
        cv.put(RssTable.COLUMN_TITLE, item.getTitle());
        cv.put(RssTable.COLUMN_DESCRIPTION, item.getDescription());
        cv.put(RssTable.COLUMN_LINK, item.getLink());
        cv.put(RssTable.COLUMN_DATE, item.getDate());
        return cv;
    }

    private RssItemConverter() { }

}
